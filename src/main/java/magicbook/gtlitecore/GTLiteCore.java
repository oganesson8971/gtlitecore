package magicbook.gtlitecore;

import magicbook.gtlitecore.api.utils.GTLCLog;
import magicbook.gtlitecore.common.CommonProxy;
import magicbook.gtlitecore.common.blocks.GTLCMetaBlocks;
import magicbook.gtlitecore.common.items.GTLCMetaItems;
import magicbook.gtlitecore.common.metatileentities.GTLCMetaTileEntities;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(
    modid = "gtlitecore",
    name = "GregTech Lite Core",
    version = "0.0.1-alpha",
    dependencies = "required-after:gregtech@[2.7.3-beta,);"
)

public class GTLiteCore {

    public static final String MODID = "gtlitecore";
    public static final String NAME = "GregTech Lite Core";
    public static final String VERSION = "0.0.1-alpha";

    @SidedProxy(
            clientSide = "magicbook.gtlitecore.client.ClientProxy",
            serverSide = "magicbook.gtlitecore.common.CommonProxy"
    )
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void onPreInit(FMLPreInitializationEvent event)
    {
        GTLCLog.init(event.getModLog());
        GTLCMetaItems.init();
        GTLCMetaTileEntities.init();
        GTLCMetaBlocks.init();
        proxy.preLoad();
    }

}

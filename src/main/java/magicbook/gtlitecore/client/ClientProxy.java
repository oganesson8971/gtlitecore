package magicbook.gtlitecore.client;

import magicbook.gtlitecore.client.textures.GTLCTextures;
import magicbook.gtlitecore.common.CommonProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber({Side.CLIENT})
public class ClientProxy extends CommonProxy {

    public ClientProxy() {
    }

    public void preLoad()
    {
        super.preLoad();
        GTLCTextures.init();
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event)
    {
        //在这里注册方块的模型
    }

}
